var express = require('express'); //Referencia al paquete express
var app = express();

// Operaciòn GET del Hola mundo
app.get('/holamundo',
  function(request, response){
    response.send('Hola Perù!');
  });

  app.listen(3003,function(){
    console.log('Node JS escuchando en el puerto 3000...');
  });
